var BOTTOM = 5;
var TOP = 25;
var attention = 0;
var meditation = 0;
var delta = 0;
var theta = 0;
var lowAlpha = 0;
var highAlpha = 0;
var lowBeta = 0;
var highBeta = 0;
var lowGamma = 0;
var highGamma = 0;

var lineArr = [];
var eegArr = [];
var MAX_LENGTH = 100;
var MAX_EEG_LENGTH = 100;
var duration = 300;
var chart = realTimeLineChart();
var eeg = realTimeLineChart();

function updateEEG(data) {
    //console.log(data);
    delta = Math.log2(data.eegPower.delta).toFixed(1);
    theta = Math.log2(data.eegPower.theta).toFixed(1);
    lowAlpha = Math.log2(data.eegPower.lowAlpha).toFixed(1);
    highAlpha = Math.log2(data.eegPower.highAlpha).toFixed(1);
    lowBeta = Math.log2(data.eegPower.lowBeta).toFixed(1);
    highBeta = Math.log2(data.eegPower.highBeta).toFixed(1);
    lowGamma = Math.log2(data.eegPower.lowGamma).toFixed(1);
    highGamma = Math.log2(data.eegPower.highGamma).toFixed(1);
    $('#delta_value').text(delta);
    $('#theta_value').text(theta);
    $('#lowalpha_value').text(lowAlpha);
    $('#highalpha_value').text(highAlpha);
    $('#lowbeta_value').text(lowBeta);
    $('#highbeta_value').text(highBeta);
    $('#lowgamma_value').text(lowGamma);
    $('#highgamma_value').text(highGamma);
}

function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

function randomNumberBounds(min, max) {
    return Math.floor(Math.random() * max) + min;
}

function seedData() {
    var now = new Date();
    for (var i = 0; i < MAX_LENGTH; ++i) {
        lineArr.push({
            time: new Date(now.getTime() - ((MAX_LENGTH - i) * duration)),
            x: randomNumberBounds(0, 0),
            y: randomNumberBounds(0, 0)
        });
    }
    for (var i = 0; i < MAX_EEG_LENGTH; ++i) {
        eegArr.push({
            time: new Date(now.getTime() - ((MAX_EEG_LENGTH - i) * duration)),
            x: randomNumberBounds(0, 0),
            y: randomNumberBounds(0, 0),
            z: randomNumberBounds(0, 0),
            k: randomNumberBounds(0, 0),
            l: randomNumberBounds(0, 0),
            m: randomNumberBounds(0, 0),
            n: randomNumberBounds(0, 0),
            p: randomNumberBounds(0, 0)
        });
    }
}

function updateData() {
    var now = new Date();
    var lineData = {
        time: now,
        x: getDataPoint('attention'),
        y: getDataPoint('meditation')
    };
    lineArr.push(lineData);
    if (lineArr.length > 30) {
        lineArr.shift();
    }
    d3.select("#chart").datum(lineArr).call(chart);
    var d = ((delta - BOTTOM) / (TOP - BOTTOM) * 100).toFixed();
    var th = ((theta - BOTTOM) / (TOP - BOTTOM) * 100).toFixed();
    var la = ((lowAlpha - BOTTOM) / (TOP - BOTTOM) * 100).toFixed();
    var ha = ((highAlpha - BOTTOM) / (TOP - BOTTOM) * 100).toFixed();
    var lb = ((lowBeta - BOTTOM) / (TOP - BOTTOM) * 100).toFixed();
    var hb = ((highBeta - BOTTOM) / (TOP - BOTTOM) * 100).toFixed();
    var lg = ((lowGamma - BOTTOM) / (TOP - BOTTOM) * 100).toFixed();
    var hg = ((highGamma - BOTTOM) / (TOP - BOTTOM) * 100).toFixed();
    var eegData = {
        time: now,
        x: (d < 0 ? 0 : d),
        y: (th < 0 ? 0 : th),
        z: (la < 0 ? 0 : la),
        k: (ha < 0 ? 0 : ha),
        l: (lb < 0 ? 0 : lb),
        m: (hb < 0 ? 0 : hb),
        n: (lg < 0 ? 0 : lg),
        o: (hg < 0 ? 0 : hg)
    };
    eegArr.push(eegData);
    if (eegArr.length > 30) {
        eegArr.shift();
    }
    d3.select("#eeg").datum(eegArr).call(eeg);
}

function resize() {
    if (d3.select("#chart svg").empty()) {
        return;
    }
    chart.width(+d3.select("#chart").style("width").replace(/(px)/g, ""));
    d3.select("#chart").call(chart);
}

var getDataPoint = function(dataType) {
    switch (dataType) {
        case 'attention':
            return attention;
            break;
        case 'meditation':
            return meditation;
            break;
    }
}

$(document).ready(function() {
    // output a simple message if WebSocket is supported by browser
    if ("WebSocket" in window) {
        console.log("WebSocket is supported by your Browser. Proceed.");
        $('#connect-controls').show();
    }

    // when "Connect" is clicked, functions to change display and start graphing data from NeuroSky
    $('#connect').on('click', function(e) {
        e.preventDefault(); // don't do what a button click normally does

        // Let us open a web socket (should be a Websocket server running locally already)
        var ws = new WebSocket("ws://127.0.0.1:50198");

        // bind send button click (send button is currently hidden, to show it, take away "hidden" class)
        $('#send-button').on('click', function(e) {
            ws.send($('#send-input').val()); // send a message back to websocket server
        });

        // when WebSocket connection is opened, do this stuff
        ws.onopen = function() {
            console.log('opened connection');

            // bind disconnect button
            $('#disconnect').off().on('click', function(e) {
                // hide disconnect button
                $(this).addClass('hidden');
                // show connect button
                $('#connect').removeClass('hidden');
                // hide send button
                $('#send-container').addClass('hidden');
                ws.close();
            });
            // hide connect button
            $('#connect').addClass('hidden');
            // show disconnect button
            $('#disconnect').removeClass('hidden');
            // show left nav tab buttons
            $('#viz-controls').removeClass('hidden');
            //$('#send-container').removeClass('hidden');
            // Web Socket is connected, send data using send()
            ws.send("Hello from websocket client!");

        };

        // whenever websocket server transmit a message, do this stuff
        ws.onmessage = function(evt) {
            // parse the data (sent as string) into a standard JSON object (much easier to use)
            if (IsJsonString(evt.data)) {

                var data = JSON.parse(evt.data);

                // handle "eSense" data
                if (data.eSense) {
                    attention = data.eSense.attention;
                    meditation = data.eSense.meditation;
                    $('#attention_value').text(attention);
                    $('#meditation_value').text(meditation);
                }
                if (data.eegPower) {
                    updateEEG(data);
                }


                //console.log(attention + '/' + meditation); // DEBUG

                // handle "blinkStrength" data
                if (data.blinkStrength) {
                    blink = data.blinkStrength;
                    //console.log('[blink] ' + blink);
                    // increment slider
                    // newVal is just cumulative added values of blink so far
                    // you can always drag the slider back to reset it
                    var newVal = parseInt($('#slider').val()) + parseInt(data.blinkStrength);
                    $('#slider').val(newVal);
                } else {
                    blink = 0;
                }

                // handle "poorSignal" data
                if (data.poorSignalLevel != null) {
                    poorSignalLevel = parseInt(data.poorSignalLevel);
                }
            } else {
                console.log(evt.data);
            }
        };

        // when websocket closes connection, do this stuff
        ws.onclose = function() {
            // websocket is closed.
            console.log("Connection is closed...");
        };
    });
});

document.addEventListener("DOMContentLoaded", function() {
    seedData();
    window.setInterval(updateData, 300);
    d3.select("#chart").datum(lineArr).call(chart);
    d3.select("#eeg").datum(eegArr).call(eeg);
    //d3.select(window).on('resize', resize);
});
