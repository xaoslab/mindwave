# Mindwave Mobile2 headset data processing on webpage

Accessing data from Mindwave Mobile2 headset via ThinkGear and node.js.

## Author

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).

## See more

[chaos lab.](https://xaoslab.tech)